<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function getString()
    {
    	$str = "Hello world";
    	return view('page', compact('str'));
    }
}
